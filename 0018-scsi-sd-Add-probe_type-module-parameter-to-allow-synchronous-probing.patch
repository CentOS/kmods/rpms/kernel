From 1d90ecbe43db0c6fad09e1c8b3ada26c5df52bf2 Mon Sep 17 00:00:00 2001
From: "Ewan D. Milne" <emilne@redhat.com>
Date: Thu, 8 Jun 2023 16:14:34 -0400
Subject: [PATCH] scsi: sd: Add "probe_type" module parameter to allow
 synchronous probing

Setting probe_type="sync" will force synchronous probing in the driver
core, which forces the ordering of minor device number assignments when
SCSI disk devices are probed, for compatibility with prior versions of RHEL.

Bugzilla: https://bugzilla.redhat.com/show_bug.cgi?id=2140017
Upstream Status: RHEL-only

Signed-off-by: Ewan D. Milne <emilne@redhat.com>
---
 drivers/scsi/sd.c | 10 ++++++++++
 1 file changed, 10 insertions(+)

diff --git a/drivers/scsi/sd.c b/drivers/scsi/sd.c
index 8947dab132d7..9586e4fcd745 100644
--- a/drivers/scsi/sd.c
+++ b/drivers/scsi/sd.c
@@ -121,6 +121,14 @@ static const char *sd_cache_types[] = {
 	"write back, no read (daft)"
 };
 
+static const char *sd_probe_types[] = { "async", "sync" };
+
+static char sd_probe_type[6] = "async";
+module_param_string(probe, sd_probe_type, sizeof(sd_probe_type),
+		    S_IRUGO|S_IWUSR);
+MODULE_PARM_DESC(probe, "async or sync. Setting to 'sync' disables asynchronous "
+		 "device number assignments (sda, sdb, ...).");
+
 static void sd_set_flush_flag(struct scsi_disk *sdkp,
 		struct queue_limits *lim)
 {
@@ -4377,6 +4385,8 @@ static int __init init_sd(void)
 		goto err_out_class;
 	}
 
+	if (!strcmp(sd_probe_type, "sync"))
+		sd_template.gendrv.probe_type = PROBE_FORCE_SYNCHRONOUS;
 	err = scsi_register_driver(&sd_template.gendrv);
 	if (err)
 		goto err_out_driver;
-- 
2.47.1

